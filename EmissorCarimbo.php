<?php

// URL PARA A EMISSÃO DE CARIMBO DO TEMPO
const URLCarimbo = "https://fw2.bry.com.br/api/carimbo-service/v1/timestamps";
// CAMINHO ONDE ESTÁ LOCALIZADO O DOCUMENTO À SER CARIMBADO
const caminhoDoDocumento = '/caminho/para/o/carimbo.png';
// HASH DO DOCUMENTO À SER CARIMBADO
const hashDoDocumento = 'hashDoDocumento';
// TOKEN AUTHORIZATION GERADO NO BRY CLOUD
const token = "tokenDeAutorização";


function EmitirCarimboArquivo() {

    echo "Inicializando emissão de carimbo em um documento \n\n";

    $documento = file_get_contents(caminhoDoDocumento);
    $documentoB64 = base64_encode($documento);
    $documentoB64Decodificado = utf8_decode($documentoB64);

    // CRIA O JSON QUE SERÁ ENVIADO NA EMISSÃO DE CARIMBO DO TEMPO
    $json = '{"nonce": "1",
            "hashAlgorithm": "SHA256",
            "format": "FILE", 
            "documents": [{"content": "' . $documentoB64Decodificado . '", 
            "nonce": "1"}]}';

    // CRIA A REQUISIÇÃO DA EMISSÃO
    $curlCarimbo = curl_init();

    curl_setopt_array($curlCarimbo, array(
    CURLOPT_URL => URLCarimbo,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS =>$json,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer " . token
    ),
));

    // ENVIA A REQUISIÇÃO PARA O BRY FRAMEWORK
    $respostaCarimbo = curl_exec($curlCarimbo);
    $httpcode = curl_getinfo($curlCarimbo, CURLINFO_HTTP_CODE);
    $respostaJson = json_decode($respostaCarimbo);
    curl_close($curlCarimbo);

    // SE A REQUISIÇÃO FOI FEITA COM SUCESSO, IMPRIME OS VALORES DO CARIMBO
    if($httpcode == 200) {

        // IMPRIME A RESPOSTA DA EMISSÃO
        echo "Resposta JSON da finalização:\n\n";
        echo $respostaCarimbo;
        echo "\n\n__________________________________________________\n\n";

        echo "Nonce do Carimbo do Tempo: " . $respostaJson->nonce . "\n\n";

        echo "Carimbo do Tempo em Base64: " . $respostaJson->timeStamps[0]->content . "\n\n";

    } else {
        echo $respostaCarimbo;

    }
}

function EmitirCarimboHash() {

    echo "Inicializando emissão de carimbo em hash do documento \n\n";

    // CRIA O JSON QUE SERÁ ENVIADO NA EMISSÃO DE CARIMBO DO TEMPO
    $json = '{"nonce": "1",
            "hashAlgorithm": "SHA256",
            "format": "HASH", 
            "documents": [{"content": "' . hashDoDocumento . '", 
            "nonce": "1"}]}';

    // CRIA A REQUISIÇÃO DA EMISSÃO
    $curlCarimbo = curl_init();

    curl_setopt_array($curlCarimbo, array(
    CURLOPT_URL => URLCarimbo,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS =>$json,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer " . token
    ),
));

    // ENVIA A REQUISIÇÃO PARA O BRY FRAMEWORK
    $respostaCarimbo = curl_exec($curlCarimbo);
    $httpcode = curl_getinfo($curlCarimbo, CURLINFO_HTTP_CODE);
    $respostaJson = json_decode($respostaCarimbo);
    curl_close($curlCarimbo);

    // SE A REQUISIÇÃO FOI FEITA COM SUCESSO, IMPRIME OS VALORES DO CARIMBO
    if($httpcode == 200) {

        // IMPRIME A RESPOSTA DA EMISSÃO
        echo "Resposta JSON da finalização:\n\n";
        echo $respostaCarimbo;
        echo "\n\n__________________________________________________\n\n";

        echo "Nonce do Carimbo do Tempo: " . $respostaJson->nonce . "\n\n";

        echo "Carimbo do Tempo em Base64: " . $respostaJson->timeStamps[0]->content . "\n\n";

    } else {
        echo $respostaCarimbo;

    }

}

EmitirCarimboArquivo();
EmitirCarimboHash();

?>